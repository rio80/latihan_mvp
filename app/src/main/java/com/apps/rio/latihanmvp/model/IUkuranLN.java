package com.apps.rio.latihanmvp.model;

public interface IUkuranLN {
    public int getKeliling();
    public int getVi();
    public int getHitung();
}
