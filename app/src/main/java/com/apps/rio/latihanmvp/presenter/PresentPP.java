package com.apps.rio.latihanmvp.presenter;

import com.apps.rio.latihanmvp.model.UkuranPP;
import com.apps.rio.latihanmvp.view.IViewPP;

public class PresentPP implements IPresentPP {

    IViewPP iViewPP;

    public PresentPP(IViewPP iViewPP) {
        this.iViewPP = iViewPP;
    }


    @Override
    public void OnHasil(int panjang, int lebar) {

        UkuranPP ukuranPP = new UkuranPP(panjang, lebar);
        iViewPP.tampilHasilPP("Luas Persegi Panjang : " + ukuranPP.getHitung());
    }
}
