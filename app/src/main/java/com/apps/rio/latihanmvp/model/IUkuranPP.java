package com.apps.rio.latihanmvp.model;

public interface IUkuranPP {
    public int getPanjang();
    public int getLebar();
    public int getHitung();
}
