package com.apps.rio.latihanmvp.model;

public class UkuranPP implements IUkuranPP {

    int panjang, lebar;

    public UkuranPP(int panjang, int lebar) {
        this.panjang = panjang;
        this.lebar = lebar;
    }

    @Override
    public int getPanjang() {
        return panjang;
    }

    @Override
    public int getLebar() {
        return lebar;
    }

    @Override
    public int getHitung() {
        return getPanjang() * getLebar();
    }
}
