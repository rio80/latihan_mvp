package com.apps.rio.latihanmvp.model;

public class UkuranLN implements IUkuranLN {

    int keliling, vi;

    public UkuranLN(int keliling, int vi) {
        this.keliling = keliling;
        this.vi = vi;
    }

    @Override
    public int getKeliling() {
        return keliling;
    }

    @Override
    public int getVi() {
        return vi;
    }

    @Override
    public int getHitung() {
        return getKeliling() * getVi();
    }
}
