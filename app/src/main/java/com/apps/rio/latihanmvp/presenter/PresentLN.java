package com.apps.rio.latihanmvp.presenter;

import com.apps.rio.latihanmvp.model.UkuranLN;
import com.apps.rio.latihanmvp.view.IViewLN;

public class PresentLN implements IPresentLN {

    IViewLN iViewLN;

    public PresentLN(IViewLN iViewLN) {
        this.iViewLN = iViewLN;
    }

    @Override
    public void onHasil(int keliling, int vi) {

        UkuranLN ukuranLN = new UkuranLN(keliling, vi);

        iViewLN.tampilHasilLN("Luas Lingkaran : " + ukuranLN.getHitung());
    }
}
