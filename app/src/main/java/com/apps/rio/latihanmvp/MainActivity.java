package com.apps.rio.latihanmvp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.apps.rio.latihanmvp.presenter.IPresentLN;
import com.apps.rio.latihanmvp.presenter.IPresentPP;
import com.apps.rio.latihanmvp.presenter.PresentLN;
import com.apps.rio.latihanmvp.presenter.PresentPP;
import com.apps.rio.latihanmvp.view.IViewLN;
import com.apps.rio.latihanmvp.view.IViewPP;

import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity implements IViewPP, IViewLN {

    EditText edtPanjang, edtLebar, edtKeliling, edtVi;
    Button btnHitungPP, btnHitungLN;
    int panjang, lebar, keliling, vi;
    IPresentPP iPresentPP;
    IPresentLN iPresentLN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtPanjang = findViewById(R.id.edt_panjang);
        edtLebar = findViewById(R.id.edt_lebar);
        edtKeliling = findViewById(R.id.edt_lebar);
        edtVi = findViewById(R.id.edt_vi);
        btnHitungPP = findViewById(R.id.btn_hitung_pp);
        btnHitungLN = findViewById(R.id.btn_hitung_ln);

        iPresentPP = new PresentPP(this);
        iPresentLN = new PresentLN(this);

        btnHitungPP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edtPanjang.getText().toString())) {
                    panjang = 0;
                } else {
                    panjang = Integer.valueOf(edtPanjang.getText().toString());
                }
                if (TextUtils.isEmpty(edtLebar.getText().toString())) {
                    lebar = 0;
                } else {
                    lebar = Integer.valueOf(edtLebar.getText().toString());
                }
                iPresentPP.OnHasil(panjang, lebar);
            }
        });

        btnHitungLN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edtKeliling.getText().toString())) {
                    keliling = 0;
                } else {
                    keliling = Integer.valueOf(edtKeliling.getText().toString());
                }
                if (TextUtils.isEmpty(edtVi.getText().toString())) {
                    vi = 0;
                } else {
                    vi = Integer.valueOf(edtVi.getText().toString());
                }
                iPresentLN.onHasil(keliling, vi);
            }
        });
    }


    @Override
    public void tampilHasilLN(String message) {
        Toasty.success(this, message, Toast.LENGTH_LONG, true).show();
    }

    @Override
    public void tampilHasilPP(String message) {
        Toasty.success(this, message, Toast.LENGTH_LONG, true).show();
    }
}
